// This is a basic Flutter widget test.

import 'package:flutter/material.dart';
import 'package:flutter_bob/main.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const String TITLE = 'Lackadaisical Bob';
  const String HINTTEXT = 'Talk to Bob';
  const String BUTTONTEXT = 'Submit';
  const String RANDOMTEXT = "The quick brown fox.";
  const String QUESTION = "What's the word?";
  const String YELLING = "LISTEN TO ME!";
  const String SAYNOTHING = '     ';
  const String BOBBASICRELPY = 'Whatever.';
  const String BOBYELLREPLY = 'Whoa, chill out!';
  const String BOBQUESTIONREPLY = 'Sure.';
  const String BOBNOTHINGREPLY = 'Fine. Be that way!';
  const String FIELDTITLE = 'Speak';
  // Create Global Finders.
  final titleFieldFinder = find.text(FIELDTITLE);
  final titleFinder = find.text(TITLE);
  final hintTextFinder = find.text(HINTTEXT);
  final buttonTextFinder = find.text(BUTTONTEXT);

  group('Home State', () {
    testWidgets('Default UI Test', (WidgetTester tester) async {
      // Build our app and trigger a frame.
      await tester.pumpWidget(MyApp());
      expect(titleFinder, findsOneWidget);
      expect(hintTextFinder, findsOneWidget);
      expect(buttonTextFinder, findsOneWidget);
      expect(find.byType(TextFormField), findsOneWidget);
      expect(titleFieldFinder, findsOneWidget);
    });
  });

  group('Responces', () {
    testWidgets('Bob' 's basic', (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());
      await tester.enterText(find.byType(TextFormField), RANDOMTEXT);
      // Tap the 'Submit' Button and trigger validation.
      await tester.tap(buttonTextFinder);
      await tester.pump();
      expect(find.text(BOBBASICRELPY), findsOneWidget);
    });

    testWidgets('Bob' 's question', (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());
      await tester.enterText(find.byType(TextFormField), QUESTION);
      // Tap the 'Submit' Button and trigger validation.
      await tester.tap(buttonTextFinder);
      await tester.pump();
      expect(find.text(BOBQUESTIONREPLY), findsOneWidget);
    });

    testWidgets('Bob' 's yell', (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());
      await tester.enterText(find.byType(TextFormField), YELLING);
      // Tap the 'Submit' Button and trigger validation.
      await tester.tap(buttonTextFinder);
      await tester.pump();
      expect(find.text(BOBYELLREPLY), findsOneWidget);
    });

    testWidgets('Bob' 's nothing', (WidgetTester tester) async {
      await tester.pumpWidget(MyApp());
      await tester.enterText(find.byType(TextFormField), SAYNOTHING);
      // Tap the 'Submit' Button and trigger validation.
      await tester.tap(buttonTextFinder);
      await tester.pump();
      expect(find.text(BOBNOTHINGREPLY), findsOneWidget);
    });
  });
}
