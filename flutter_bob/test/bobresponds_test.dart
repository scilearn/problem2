import 'package:flutter_bob/bobtalk/bobsresponds.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  const String RANDOMTEXT = "The quick brown fox.";
  const String QUESTION = "What's the word?";
  const String YELLING = "LISTEN TO ME!";
  const String SAYNOTHING = '     ';
  const String BOBBASICRELPY = 'Whatever.';
  const String BOBYELLREPLY = 'Whoa, chill out!';
  const String BOBQUESTIONREPLY = 'Sure.';
  const String BOBNOTHINGREPLY = 'Fine. Be that way!';

  test('Basic Sentence', () {
    expect(BobResponds().hey(RANDOMTEXT), BOBBASICRELPY);
  });

  test('Yell at Bob', () {
    expect(BobResponds().hey(YELLING), BOBYELLREPLY);
  });

  test('Question Bob', () {
    expect(BobResponds().hey(QUESTION), BOBQUESTIONREPLY);
  });

  test('Address Bob with nothing', () {
    expect(BobResponds().hey(SAYNOTHING), BOBNOTHINGREPLY);
  });
}
