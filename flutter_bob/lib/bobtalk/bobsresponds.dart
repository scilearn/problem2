class BobResponds {
  // Bob's replies to hey
  String hey(String statement) {
    if (_isAddressing(statement) != null) return _isAddressing(statement);
    if (_isYelling(statement) != null) return _isYelling(statement);
    if (_isQuestion(statement) != null) return _isQuestion(statement);
    return 'Whatever.';
  }

  String _isQuestion(String statement) {
    if (statement.endsWith('?'))
      return 'Sure.';
    else
      return null;
  }

  String _isYelling(String statement) {
    if (statement == statement.toUpperCase())
      return 'Whoa, chill out!';
    else
      return null;
  }

  String _isAddressing(String statement) {
    //addressing bob with nothing
    if (statement.trim() == '')
      return 'Fine. Be that way!';
    else
      return null;
  }
}
