import 'package:flutter/material.dart';

import 'bobtalk/bobsresponds.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lackadaisical Bob',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Lackadaisical Bob'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of the application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: TalkToBobForm(),
    );
  }
}

// A form to collect the statement to Bob
class TalkToBobForm extends StatefulWidget {
  @override
  TalkToBobFormState createState() {
    return TalkToBobFormState();
  }
}

class TalkToBobFormState extends State<TalkToBobForm> {
  //Global key that  identifies the Form widget and allows validations.
  final _formKey = GlobalKey<FormState>();
  final _sentenceController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    //A Form widget using the _formKey
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          TextFormField(
            controller: _sentenceController,
            decoration: InputDecoration(
              border: InputBorder.none,
              hintText: "Talk to Bob",
              labelText: 'Speak',
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 16.0),
            child: RaisedButton(
              onPressed: () {
                String bobsreply = BobResponds().hey(_sentenceController.text);
                // Display Bob's reply in a Snackbar.
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text(bobsreply)));
              },
              child: Text('Submit'),
            ),
          ),
        ],
      ),
    );
  }
}
